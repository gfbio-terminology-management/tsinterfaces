package org.gfbio.wrappers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

/**
 * Class to extract information about ontology syntaxes, ontology formality levels,
 * ontology tasks, ontology domains and ontology types from virtuoso.
 * @author Alexander Hinze-Huettl
 */
public class OmvInfo {
	private static String hostURL = "jdbc:virtuoso://" + Settings.getVirtIP() + ":1111";
	private static String userName = Settings.getVirtUser();
	private static String password = Settings.getVirtPassword();

	private VirtGraph set = new VirtGraph(hostURL, userName, password);

	public List<OmvIndividual> getOntologySyntaxes(){
		return this.getOmvIndividuals("OntologySyntax");
	}

	public List<OmvIndividual> getOntologyFormalityLevels(){
		List<OmvIndividual> formalityLevels = this.getOmvIndividuals("FormalityLevel");
		Collections.reverse(formalityLevels);
		return formalityLevels;
	}

	public List<OmvIndividual> getOntologyTasks(){
		return this.getOmvIndividuals("OntologyTask");
	}

	public List<OmvIndividual> getOntologyDomains(){
		List<OmvIndividual> ontologyDomains = this.getOmvIndividuals("OntologyDomain");
		
		Collections.sort(ontologyDomains, new Comparator<OmvIndividual>(){
			@Override
			public int compare(OmvIndividual o1, OmvIndividual o2) {
				return o1.getLabel().compareTo(o2.getLabel());
			}});
		
		return ontologyDomains;
	}

	public List<OmvIndividual> getOntologyTypes(){
		return this.getOmvIndividuals("OntologyType");
	}

	public List<String> getOntologyDomainKeywords(String ontologyDOmainUri){
		String query = "select * WHERE { GRAPH ?graph{ \n";
		query += "<" + ontologyDOmainUri + "> <http://omv.ontoware.org/2005/05/ontology#keywords> ?keywords\n";
		query += "}}";

		String regexPattern = "(.+)\\^\\^.+";
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
		ResultSet result = vqe.execSelect();
		List<String> keywords = new ArrayList<String>();

		if(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			String keywordList = sol.get("keywords").toString();
			Pattern p = Pattern.compile(regexPattern);
			Matcher m = p.matcher(keywordList);
			System.out.println(keywordList);
			if(m.matches())
				for(String keyword : m.group(1).split(",")){
					if(!keyword.trim().isEmpty())
						keywords.add(keyword);
					System.out.println(keyword);
				}
		}
		return keywords;
	}
	
	/**
	 * Extract a map with all individuals of the given class name.
	 * The map contains uri and label pairs from these individuals.
	 * @param className
	 * @retur
	 */
	private List<OmvIndividual> getOmvIndividuals(String className) {
		//Sparql query to get all individuals and thier labels
		String query = "select * WHERE{ GRAPH ?graph {\n"
						+ "?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type>\n"
						+ " <http://omv.ontoware.org/2005/05/ontology#" + className + "> .\n"
						+ "?uri rdfs:label ?label\n"
						+ "}}";
		//Execute the query and extract uri and label by regex
		String regexPattern = "(.+)\\^\\^.+";
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
		ResultSet result = vqe.execSelect();
		List<OmvIndividual> omvIndividuals = new ArrayList<OmvIndividual>();
		while (result.hasNext()) {
			QuerySolution sol = result.nextSolution();
			String url = sol.get("uri").toString();
			String label = sol.get("label").toString();

			Pattern p = Pattern.compile(regexPattern);
			Matcher m = p.matcher(label);
			if(m.matches())
				omvIndividuals.add(new OmvIndividual(url, m.group(1)));
		}
		return omvIndividuals;
	}
	
	public class OmvIndividual {
		private String uri;
		private String label;
		
		
		public OmvIndividual(String uri, String label) {
			super();
			this.uri = uri;
			this.label = label;
		}
		public String getUri() {
			return uri;
		}
		public void setUri(String uri) {
			this.uri = uri;
		}
		public String getLabel() {
			return label;
		}
		public void setLabel(String label) {
			this.label = label;
		}
	};

}
