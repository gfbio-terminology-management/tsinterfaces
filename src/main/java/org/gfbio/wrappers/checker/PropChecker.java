package org.gfbio.wrappers.checker;


import java.io.IOException;
import java.util.Map;

import org.gfbio.wrappers.Settings;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;


public abstract class PropChecker {
	private static String hostURL = "jdbc:virtuoso://" + Settings.getVirtIP() + ":1111";
	private static String userName = Settings.getVirtUser();
	private static String password = Settings.getVirtPassword();
	private VirtGraph set = new VirtGraph(hostURL, userName, password);

	/**
	 * Status of a defect property.
	 * NO_LABEL: Property was declared in ontology without label annotation
	 * NOT_DECLARED: Property was not declared in ontology and not found in virtuoso
	 */
	public static enum PropertyStatus{
		NO_LABEL,
		NOT_DECLARED
	}

	/**
	 * get the graph where a property is stored
	 * @param propertyUri
	 * @return null if property was not found
	 */
	protected String getGraph(String propertyUri){
		//TODO check all properties in one query, do not query each property!
		String query = "SELECT * where { GRAPH ?graph {\n"  
					 + "<" +propertyUri + "> ?a ?b\n"
					 + "}}";
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
		ResultSet result = vqe.execSelect();
		if(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			return sol.get("?graph").toString();
		}
		return null;
	}

	protected String resolveNameSpaceAndAttribute(String filename, String namespace_attribute){
		String elements[] = namespace_attribute.split(":");
		if(elements.length == 2){
			return resolveNameSpaceAndAttribute(filename,elements[0], elements[1]);
		}
		return null;
	}

	protected String resolveNameSpaceAndAttribute(String filename,String namespace, String attribute){
		SAXBuilder builder = new SAXBuilder();
		Document doc;
		try {
			doc = (Document) builder.build(filename);
	        String nsUri =doc.getRootElement().getNamespace(namespace).getURI();
	        return nsUri + attribute;
		} catch (JDOMException | IOException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public abstract Map<String,PropertyStatus> getProperties(String filename,String labelProp);
	
}
