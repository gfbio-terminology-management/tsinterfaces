package org.gfbio.wrappers.checker;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;


public class OWLPropertyChecker extends PropChecker {
	HashSet<String> knownVirtProperties = new HashSet<String>();
	
	/**
	 * collect all object, data and annotation properties
	 * @param ontology
	 * @return
	 */
	private HashSet<OWLEntity> collectProperties(OWLOntology ontology){
		HashSet<OWLEntity> result = new HashSet<OWLEntity>();
		for(OWLDataProperty prop : ontology.getDataPropertiesInSignature()){
			if(!result.contains(prop)){
				result.add(prop);
			}
		}

		for(OWLObjectProperty prop : ontology.getObjectPropertiesInSignature()){
			//System.out.println("-" + prop.toString());
			if(!result.contains(prop)){
				result.add(prop);
			}
		}

		for(OWLAnnotationProperty prop : ontology.getAnnotationPropertiesInSignature()){
			if(!result.contains(prop)){
				result.add(prop);
			}
		}
		return result;
	}

	/**
	 * load an owl file
	 * @param ontology_url
	 * @return
	 */
	private OWLOntology loadOWL(String ontology_url) {
		OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
		IRI iri = IRI.create(ontology_url);
		try {
			return manager.loadOntologyFromOntologyDocument(iri);
		} catch (OWLOntologyCreationException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * get all properties from an ontology.
	 * @param filename path to the file (e.g. file:///foo/bar.owl)
	 * @param labelProp label property to check if property was annotated by label
	 * @return Map of defect properties and their status (not declared or 
	 */
	public  Map<String,PropertyStatus> getProperties(String filename,String labelProp){
		OWLOntology ontology = null;
		if(!labelProp.startsWith("http://")){
			labelProp =  this.resolveNameSpaceAndAttribute(filename, labelProp);
		}
		if(filename.endsWith("owl")){
			ontology = loadOWL(filename);
		}else{
			throw new RuntimeException("Bad file extension");
		}
		return this.getProperties(ontology, labelProp);
	}

	/**
	 * get all properties from an ontology.
	 * @param ontology
	 * @param labelProp label property to check if property was annotated by label
	 * @return Map of defect properties and their status (not declared or 
	 */
	private Map<String,PropertyStatus> getProperties(OWLOntology ontology,String labelProp){
		Map<String,PropertyStatus> result = new HashMap<String,PropertyStatus>();
		HashSet<OWLEntity> properties = collectProperties(ontology);

		for(OWLEntity prop : properties){
			if(prop.isBuiltIn())
				continue;
			if(result.containsKey(prop.getIRI().toString()))
				continue;

			boolean declaredInOntology = ontology.isDeclared(prop,false);
			
			boolean inVirt = false;
			if(!knownVirtProperties.contains(prop.getIRI().toString())){
				if(getGraph(prop.getIRI().toString()) != null){
					knownVirtProperties.add(prop.getIRI().toString());
					inVirt = true;
				}
			}

			if(!declaredInOntology && !inVirt){
				result.put(prop.getIRI().toString(), PropertyStatus.NOT_DECLARED);
			}else if(declaredInOntology){
				boolean hasLabel = false;
				for(OWLAnnotation anno : prop.getAnnotations(ontology)){
					if(anno.toString().contains(labelProp)){
						hasLabel = true;
						break;
					}
				}
				if(!hasLabel){
					result.put(prop.getIRI().toString(), PropertyStatus.NO_LABEL);
				}
			}
		}
		return result;
	}

	public static void main(String a[]){
		OWLPropertyChecker o = new OWLPropertyChecker();
		Map<String,PropertyStatus> foo = o.getProperties("file:///home/alex/Downloads/bco.owl", "rdfs:label");
		for(Entry<String,PropertyStatus> bar : foo.entrySet()){
			System.out.println(bar.getValue() + " : " + bar.getKey());
		}
	}
}
