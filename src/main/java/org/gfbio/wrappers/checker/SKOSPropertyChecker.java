package org.gfbio.wrappers.checker;

import java.net.URI;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;

import org.semanticweb.skos.SKOSAnnotation;
import org.semanticweb.skos.SKOSConcept;
import org.semanticweb.skos.SKOSConceptScheme;
import org.semanticweb.skos.SKOSCreationException;
import org.semanticweb.skos.SKOSDataset;
import org.semanticweb.skosapibinding.SKOSManager;
import org.semanticweb.skosapibinding.SKOStoOWLConverter;

import uk.ac.manchester.cs.skos.SKOSDatasetImpl;

public class SKOSPropertyChecker extends PropChecker {
	SKOStoOWLConverter skos2owlConverter = new SKOStoOWLConverter();
	HashSet<String> knownConceptsAndShemas = new HashSet<String>();
	/**
	 * load an skos/rdf file
	 * @param ontology_url
	 * @return
	 */
	private SKOSDataset loadSKOS(String ontology_url){
	    try{
	        SKOSManager manager = new SKOSManager();

	        return manager.loadDataset(URI.create(ontology_url));

	      } catch (SKOSCreationException e) {
	      e.printStackTrace();
	    }
	    return null;
	}

	private Map<String, PropertyStatus> getProperties(SKOSDatasetImpl dataset, String labelProp){
		//Have a look at http://sourceforge.net/p/skosapi/code/HEAD/tree/version_3/src/uk/ac/manchester/cs/skos/SKOSDatasetImpl.java
		Map<String, PropertyStatus> result = new HashMap<String,PropertyStatus>();

		for(SKOSConceptScheme scheme : dataset.getSKOSConceptSchemes()){
			if(!knownConceptsAndShemas.contains(scheme.getURI().toString()))
				knownConceptsAndShemas.add(scheme.getURI().toString());
		}

		for(SKOSConcept concept : dataset.getSKOSConcepts()){
			if(!knownConceptsAndShemas.contains(concept.getURI().toString()))
				knownConceptsAndShemas.add(concept.getURI().toString());
		}

		for(SKOSConcept concept : dataset.getSKOSConcepts()){
			boolean hasLabel = false;
			for (SKOSAnnotation annotation : dataset.getSKOSAnnotations(concept)) {
				String uri = annotation.getURI().toString();

				if(!hasLabel && uri.contains(labelProp)){
					hasLabel = true;
					continue;
				}
				if(annotation.isAnnotationByConstant())
					continue;

				if(!knownConceptsAndShemas.contains(uri) && !result.containsKey(uri)){
					if(getGraph(uri) == null){
						result.put(uri, PropertyStatus.NOT_DECLARED);
					}else{
						knownConceptsAndShemas.add(uri);
					}
				}
			}
			if(!hasLabel){
				result.put(concept.getURI().toString(), PropertyStatus.NO_LABEL);
			}	
		}
		return result;
	}

	@Override
	public Map<String, PropertyStatus> getProperties(String filename,
			String labelProp) {
		if(!labelProp.startsWith("http://")){
			labelProp =  this.resolveNameSpaceAndAttribute(filename, labelProp);
		}
		SKOSDatasetImpl dataset = (SKOSDatasetImpl)loadSKOS(filename);
		return getProperties(dataset,labelProp);
	}
	
	public static void main(String a[]){
		SKOSPropertyChecker s = new SKOSPropertyChecker();
		Map<String, PropertyStatus> foo = s.getProperties("file:///home/alex/Downloads/iso3166_countries_and_subdivisions.rdf","http://www.w3.org/2004/02/skos/core#prefLabel");
		for(Entry<String,PropertyStatus> bar : foo.entrySet()){
			System.out.println(bar.getValue() + " : " + bar.getKey());
		}
	}
}
