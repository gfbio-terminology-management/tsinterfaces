package org.gfbio.wrappers;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 * Class to handle ontologies in admin interface
 * @author Alexander Hinze-Huettl
 *
 */
public class OntologyInterface {
	private String hostURL = "jdbc:virtuoso://" + Settings.getVirtIP() + ":1111";
	private String userName = Settings.getVirtUser();
	private String password = Settings.getVirtPassword();

	private VirtGraph set = new VirtGraph(hostURL, userName, password);

	private ResultSet executeQuery(String query){
		VirtuosoQueryExecution vqe = VirtuosoQueryExecutionFactory.create(query, set);
		return vqe.execSelect();
	}

	/**
	 * Remove graph from virtuoso
	 * @param graphName
	 */
	public void deleteGraph(String graphName){
		String query = "DROP SILENT GRAPH <" + graphName + ">";
		this.executeQuery(query);
	}

	/**
	 * Remove all metadata information about an ontology from metadata graph
	 * @param hashUri hashUri hashed acronym (e.g. http://purl.gfbio.org/487849613 for BEFDATA)
	 */
	public void deleteMetadata(String hashUri){
		String query;
		query = "WITH <http://purl.gfbio.org/Metadata>\n";
		query += "DELETE{ <" + hashUri + "> ?p ?o}";
		query += "WHERE { <" + hashUri + "> ?p ?o}";
		this.executeQuery(query);
	}

	/**
	 * Get all ontologies listen in Metadata graph http://purl.gfbio.org/Metadata.
	 * @return map where KEY is the hash uri (e.g.http://purl.gfbio.org/487849613) from the ontology and VALUE is the acronym (e.g. BEFDATA)
	 */
	public Map<String,String> listOntologies(){
		String query;
		query = "SELECT ?uri ?acronym from <http://purl.gfbio.org/Metadata>\n";
		query += "WHERE { ?uri <http://www.w3.org/1999/02/22-rdf-syntax-ns#type> ?type .\n";
		query += " ?uri <http://omv.ontoware.org/ontology#acronym> ?acronym}";
		ResultSet result = this.executeQuery(query);
		Map<String, String> ontologies = new HashMap<String,String>();
		while(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			String uri = sol.get("uri").toString();
			String acronym = sol.get("acronym").toString();
			ontologies.put(uri, acronym);
		}
		return ontologies;
	}

	/**
	 * Get graph from ontology by its hash from metadata.
	 * E.g. http://purl.gfbio.org/487849613 will be resolved to http://purl.gfbio.org/BEFDATA
	 * @param uriHash hashUri hashUri hashed acronym (e.g. http://purl.gfbio.org/487849613 for BEFDATA)
	 * @return
	 */
	public String getGraphFromHash(String uriHash){
		String query;
		query = "SELECT ?graph FROM <http://purl.gfbio.org/Metadata>";
		query += "WHERE{<" + uriHash + "> <http://purl.org/gfbio/metadata-schema#graph> ?graph}";
		ResultSet result = this.executeQuery(query);
		System.out.println(query);
		if(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			return sol.get("graph").toString();
		}
		return null;
	}
	
	/**
	 * Get all metadata information about an ontology
	 * @param hashUri hashed acronym (e.g. http://purl.gfbio.org/487849613 for BEFDATA)
	 * @return Map where KEY is the property (e.g. http://omv.ontoware.org/ontology#acronym) and VALUE is the object (e.g. BEFDATA)
	 */
	public Map<String,String> getMetadata(String hashUri){
		String query;
		query = "SELECT ?p ?o from <http://purl.gfbio.org/Metadata>\n";
		query += "WHERE { <" + hashUri + "> ?p ?o}";
		ResultSet result = this.executeQuery(query);
		Map<String, String> metadata = new HashMap<String,String>();
		while(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			String property = sol.get("p").toString();
			String object = sol.get("o").toString();
			metadata.put(property, object);
		}
		return metadata;
	}

	public boolean deleteResourceFile(String uriHash){
		String query;
		query = "SELECT ?resource FROM <http://purl.gfbio.org/Metadata>";
		query += "WHERE {<" + uriHash + "> omv:resourceLocator ?resource}";
		ResultSet result = this.executeQuery(query);
		if(result.hasNext()){
			QuerySolution sol = result.nextSolution();
			File f = new File(sol.get("resource").toString());
			return f.delete();
		}
		return false;
	}
}
