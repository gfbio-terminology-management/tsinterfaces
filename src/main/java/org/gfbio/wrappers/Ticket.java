package org.gfbio.wrappers;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.gfbio.terminologies.metrics.GFBioMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioOWLMetricsCalculator;
import org.gfbio.terminologies.metrics.GFBioSKOSMetricsCalculator;
import org.gfbio.wrappers.checker.OWLPropertyChecker;
import org.gfbio.wrappers.checker.PropChecker;
import org.gfbio.wrappers.checker.PropChecker.PropertyStatus;
import org.gfbio.wrappers.checker.SKOSPropertyChecker;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.UnloadableImportException;

import com.hp.hpl.jena.util.FileUtils;

/**
 * This class represents a ticket which holds informations about an uploaded
 * ontology.
 * @author Alexander Hinze-Huettl
 * @author Alexandra La Fleur
 */
public class Ticket implements Serializable {

	private String ontology_filename;
	private String ticket_location;

	private String ontology_name;
	private String ontology_uri;
	private String ontology_description;
	private String ontology_acronym;

	private String ontology_version;

	private Date ontology_creation_date;
	private Date ontology_release_date;

	private String ontology_status;
	private String[] ontology_domains;
	private String[] ontology_keywords;
	private String[] ontology_creators;
	private String[] ontology_contributors;
	private String ontology_language;

	private String ontology_contact_firstname;
	private String ontology_contact_lastname;
	private String ontology_contact_email;

	private String ontology_homepage;
	private String ontology_documentation;
	private String ontology_format_language;

	// Metric fields
	private Integer ontology_number_of_classes;
	private Integer ontology_number_of_individuals;
	private Integer ontology_number_of_properties;
	private Integer ontology_maximum_depth;
	private Integer ontology_maximum_number_of_children;
	private Integer ontology_average_number_of_children;
	private Integer ontology_classes_single_child;
	private Integer ontology_classes_more_25_children;
	private Integer ontology_classes_without_definition;
	private Integer ontology_number_of_leaves;
	private Integer ontology_classes_without_label;
	private Integer ontology_classes_with_more_1_parent;
	private String ontology_dl_expressivity;
	
	private String ontology_format_label;
	private String ontology_format_synonym;
	private String ontology_format_definition;
	private String ontology_format_formality_level;
	
	private Boolean _metricsCalculated;
	private Boolean _propertiesChecked;
	private boolean backwardCompatible;
	private boolean inCompatible;
	
	private Map<String, PropertyStatus> badProperties;
	
	private Map<String,Object> oldVersionData;
	private Set<String> removedClasses;
	/**
	 * Create a new ticket by given http request. The constructor will parse the
	 * request and extract information from the ontology.
	 */
	public Ticket(HttpServletRequest request) {
		_metricsCalculated = false;
		_propertiesChecked = false;
		HttpSession session = request.getSession(false);
		if (session == null || !request.isRequestedSessionIdValid()
				|| session.getAttribute("filename") == null) {
			throw new RuntimeException("session timeout"); // TODO implements
															// GFBioSessionTomeoutException
		}
		ontology_filename = session.getAttribute("filename").toString();
		Map<String, String[]> parameterMap = request.getParameterMap();
		parseUserParameters(request);
	}

	/**
	 * If user made some changes, this method updates values.
	 * 
	 * @param request
	 * @return
	 */
	public boolean update(HttpServletRequest request) {
		// extract informations from request and override all old values
		parseUserParameters(request);
		// delete the old serialized file
		boolean fileDeleted = this.deleteTicketFile();
		// serialize again and store the new values
		boolean filesSaved = this.save(this.ticket_location);
		return fileDeleted & filesSaved;
	}

	/**
	 * Store the ticked as serialized java object.
	 * 
	 * @param filename
	 * @return
	 */
	public boolean save(String filename) {
		this.ticket_location = filename;
		ObjectOutputStream oos = null;
		try {
			oos = new ObjectOutputStream(new FileOutputStream(filename));
			oos.writeObject(this);
			oos.close();
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Delete the serialized ticket and the depending ongology
	 */
	public boolean deleteFiles() {
		return deleteTicketFile() & deleteOntologyFile();
	}

	/**
	 * Delete the ticket file only
	 */
	public boolean deleteTicketFile() {
		return (new File(this.ticket_location)).delete();
	}

	/**
	 * Delete tickets ontology file only
	 */
	public boolean deleteOntologyFile() {
		return (new File(this.ontology_filename)).delete();
	}

	/**
	 * Extract all information/metadata from request and store them.
	 * 
	 * @param request
	 *            Http request with parameters to extract information
	 */
	private void parseUserParameters(HttpServletRequest request) {
		ontology_acronym = request.getParameter("ontology_acronym")
				.toUpperCase();
		if (ontology_acronym == null || ontology_acronym.isEmpty())
			throw new RuntimeException("unspecified parameter ontology_acronym"); // TODO
																					// implements
																					// GFBioBadFormatException

		ontology_name = request.getParameter("ontology_name");
		if (ontology_name == null || ontology_name.isEmpty())
			throw new RuntimeException("unspecified parameter ontology_name"); // TODO
																				// implements
																				// GFBioBadFormatException

		ontology_uri = request.getParameter("ontology_uri");
		if (ontology_uri == null || ontology_uri.isEmpty())
			throw new RuntimeException("unspecified parameter ontology_uri"); // TODO
																				// implements
																				// GFBioBadFormatException

		ontology_description = request.getParameter("ontology_description");
		if (ontology_description == null || ontology_description.isEmpty())
			throw new RuntimeException(
					"unspecified parameter ontology_description"); // TODO
																	// implements
																	// GFBioBadFormatException

		
		ontology_version = request.getParameter("ontology_version");
		ontology_status = request.getParameter("ontology_status");
		
		if(request.getParameter("ontology_domains") != null) {
			ontology_domains = request.getParameter("ontology_domains").split(",");
		}

		ontology_creation_date = stringToDate(request
				.getParameter("ontology_creation_date"));
		ontology_release_date = stringToDate(request
				.getParameter("ontology_release_date"));

		if (request.getParameter("ontology_keywords") != null)
			ontology_keywords = request.getParameter("ontology_keywords")
					.split(",");

		if (request.getParameter("ontology_contributors") != null)
			ontology_contributors = request.getParameter(
					"ontology_contributors").split(",");

		if (request.getParameter("ontology_creators") != null)
			ontology_creators = request.getParameter("ontology_creators")
					.split(",");

		ontology_language = request.getParameter("ontology_language");

		ontology_format_label = request.getParameter("ontology_format_label");
		ontology_format_synonym = request
				.getParameter("ontology_format_synonym");
		ontology_format_definition = request
				.getParameter("ontology_format_definition");
		ontology_format_formality_level = request
				.getParameter("ontology_format_formality_level");
		ontology_format_language = request.getParameter("ontology_format_language");
		
		ontology_contact_firstname = request.getParameter("ontology_contact_firstname");
		ontology_contact_lastname = request.getParameter("ontology_contact_lastname");
		ontology_contact_email = request.getParameter("ontology_contact_email");

		ontology_homepage = request.getParameter("ontology_homepage");
		ontology_documentation = request.getParameter("ontology_documentation");
		

		String compatibility = request.getParameter("compatibility");
		if (("backwardCompatible").equals(compatibility)){
			backwardCompatible = true;
		}
		else if (("incompatible").equals(compatibility)){
			inCompatible = true;
		}
		System.out.println(this);
	}

	/**
	 * Convert string with MM/dd/yy format into an Date object
	 */
	private Date stringToDate(String dStr) {
		if (dStr == null || dStr.trim().isEmpty())
			return null;
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
		try {
			return df.parse(dStr);
		} catch (ParseException e) {
			return null;
		}
	}

	/**
	 * Calculate ontologys metrics with GFBioMetricsCalculator. Metrics will be
	 * calculated only once.
	 */
	public void calculateMetrics() throws UnloadableImportException, OWLOntologyCreationException{
		// If metrics were calculated in the past, dont calculate them again
		if (this._metricsCalculated)
			return;
		GFBioMetricsCalculator calculator = null;
		String extension = FilenameUtils.getExtension(this.ontology_filename);
		// Different file types need different metric calculators:

		if (this.ontology_format_language.equals("http://omv.ontoware.org/ontology#OWL")) {
			calculator = new GFBioOWLMetricsCalculator("file:///"
						+ this.ontology_filename);
		} else if (this.ontology_format_language.equals("http://purl.org/gfbio/metadata-schema#SKOS")) {
			calculator = new GFBioSKOSMetricsCalculator("file:///"
					+ this.ontology_filename);
		} else {
			// TODO throw a better exception
			throw new RuntimeException("Unknown fileformat " + extension
					+ " for file " + this.ontology_filename);
		}
		calculator.setDefinition(this.ontology_format_definition);
		calculator.setLabel(this.ontology_format_label);
		calculator.calculateMetrics();
		_metricsCalculated = true;
		this.ontology_number_of_classes = calculator.getClassesCount();
		this.ontology_number_of_individuals = calculator.getIndividualsCount();
		this.ontology_number_of_properties = calculator.getPropertiesCount();
		this.ontology_maximum_depth = calculator.getMaxDepth();
		this.ontology_maximum_number_of_children = calculator.getMaxChildren();
		this.ontology_average_number_of_children = calculator.getAvgChildren();
		this.ontology_classes_single_child = calculator.getSingleChild();
		this.ontology_classes_more_25_children = calculator.getMore25Children();
		this.ontology_classes_without_definition = calculator
				.getClassesWithoutDefinition();
		this.ontology_number_of_leaves = calculator.getNumberOfLeaves();
		this.ontology_classes_without_label = calculator
				.getClasesWithoutLabel();
		this.ontology_classes_with_more_1_parent = calculator
				.getClassesWithMoreThan1Parent();
		this.ontology_dl_expressivity = calculator.getDescriptionLogicName();
	}

	public String toString() {
		String result = "Ticket{";
		result += "ontology_location=" + ontology_filename + "; ";
		result += "ontology_name=" + ontology_name + "; ";
		result += "ontology_uri=" + ontology_uri + "; ";
		result += "ontology_desription=" + ontology_description + "; ";
		result += "ontology_acronym=" + ontology_acronym + "; ";

		result += "ontology_version=" + ontology_version + "; ";
		result += "ontology_status=" + ontology_status + "; ";
	
		if (ontology_creation_date == null){
			result += "ontology_creation_date=NULL";
		}else {
		result += "ontology_creation_date=" + ontology_creation_date.toString()
				+ "; ";
		}
		result += "ontology_release_date=" + ontology_release_date.toString()
				+ "; ";

		if(ontology_domains != null){
			result += "ontology_domains=[";
			for (String domain : ontology_domains)
				result += domain + "; ";
			result += "]; ";
		}else{
			result += "ontology_domains=NULL;";
		}
		
		if(ontology_keywords != null){
			result += "ontology_keywords=[";
			for (String keyword : ontology_keywords)
				result += keyword + "; ";
			result += "]; ";
		}else{
			result += "ontology_keywords=NULL";
		}
		if(ontology_creators != null){
			result += "ontology_creators=[";
			for (String creator : ontology_creators)
				result += creator + "; ";
			result += "]; ";
		}else{
			result += "ontology_creators=NULL;";
		}
		
		result += "ontology_language=" + ontology_language + "; ";

		if(ontology_contributors != null){
			result += "ontology_contributors=[";
			for (String con : ontology_contributors)
				result += con + "; ";
			result += "]; ";
		}else{
			result += "ontology_contributors=NULL;";
		}

		result += "ontology_format_label=" + ontology_format_label + "; ";
		result += "ontology_format_synonym=" + ontology_format_synonym + "; ";
		result += "ontology_format_definition=" + ontology_format_definition
				+ "; ";
		result += "ontology_format_language= "+ ontology_format_language+"; ";
		result += "ontology_format_formality_level="
				+ ontology_format_formality_level + "; ";

		result += "ontology_contact_firstname=" + ontology_contact_firstname + "; ";
		result += "ontology_contact_lastname=" + ontology_contact_lastname + "; ";
		result += "ontology_contact_email=" + ontology_contact_email + "; ";

		result += "ontology_homepage=" + ontology_homepage + "; ";
		result += "ontology_documentation=" + ontology_documentation + "; ";

		result += "}";

		return result;
	}
	
	// getter methods
	public String getOntologyFileLocation() {
		return this.ontology_filename;
	}

	public String getOntologyVersoningLocation(){
		String filename = Settings.getOntologyStorePath();
		filename += this.getOntologyAcronym() + "/";
		filename += (new SimpleDateFormat("dd-MM-yyyy").format(this.getOntologyRealeaseDate())) + "/";
		filename += (FileUtils.getBasename(this.getOntologyFileLocation()));
		return filename;
	}

	public String getTicketFileLocation() {
		return this.ticket_location;
	}

	public String getOntologyName() {
		return this.ontology_name;
	}

	public String getOntologyAcronym() {
		return this.ontology_acronym;
	}

	public String getOntologyUri() {
		return this.ontology_uri;
	}

	public String getOntologyDescription() {
		return this.ontology_description;
	}

	public String getOntologyVersion() {
		return this.ontology_version;
	}

	public String getOntologyStatus() {
		return this.ontology_status;
	}

	public String[] getOntologyDomains() {
		return this.ontology_domains;
	}

	public String[] getOntologyKeywords() {
		return this.ontology_keywords;
	}

	public String[] getOntologyContributors() {
		return this.ontology_contributors;
	}

	public String getOntologyLanguage() {
		return this.ontology_language;
	}

	public String getOntologyContactFirstName() {
		return this.ontology_contact_firstname;
	}

	public String getOntologyContactLastName() {
		return this.ontology_contact_lastname;
	}
	
	public String getOntologyContactEmail() {
		return this.ontology_contact_email;
	}

	public String getOntologyHomepage() {
		return this.ontology_homepage;
	}

	public String getOntologyDocumentation() {
		return this.ontology_documentation;
	}

	public int getOntologyNumberOfClasses() {
		return this.ontology_number_of_classes;
	}

	public int getOntologyNumberOfIndividuals() {
		return this.ontology_number_of_individuals;
	}

	public int getOntologyNumberOfProperties() {
		return this.ontology_number_of_properties;
	}

	public int getOntologyMaximumDepth() {
		return this.ontology_maximum_depth;
	}

	public int getOntologyMaximumNumberOfChildren() {
		return this.ontology_maximum_number_of_children;
	}

	public int getOntologyAverageNumberOfChildren() {
		return this.ontology_average_number_of_children;
	}

	public int getOntologyClassesSingleChild() {
		return this.ontology_classes_single_child;
	}

	public int getOntologyClassesMore25Children() {
		return this.ontology_classes_more_25_children;
	}
	
	public int getOntologyClassesWithoutDefinition() {
		return this.ontology_classes_without_definition;
	}
	
	public String getDlExpressivity(){
		return this.ontology_dl_expressivity;
	}

	public String getOntologyFormatLanguage(){
		return this.ontology_format_language;
	}

	public String getOntologyFormatFormalitLevel() {
		return this.ontology_format_formality_level;
	}

	public boolean metricsCalculated() {
		return this._metricsCalculated;
	}

	public Date getOntologyCreationDate() {
		return this.ontology_creation_date;
	}

	public Date getOntologyRealeaseDate() {
		return this.ontology_release_date;
	}

	public String[] getOntologyCreators() {
		return this.ontology_creators;
	}

	public String getOntologyFormatLabel() {
		return this.ontology_format_label;
	}

	public String getOntologyFormatSynonym() {
		return this.ontology_format_synonym;
	}

	public String getOntologyFormatDefinition() {
		return this.ontology_format_definition;
	}

	public Integer getOntologyNumberOfLeaves() {
		return this.ontology_number_of_leaves;
	}

	public Integer getOntologyClassesWithoutLabel() {
		return this.ontology_classes_without_label;
	}

	public Integer getOntologyClassesWithMoreThan1Parent() {
		return this.ontology_classes_with_more_1_parent;
	}

	public Map<String,PropertyStatus> getBadProperties(){
		if(this._propertiesChecked == false){
			PropChecker checker;
			if(this.ontology_filename.endsWith(".owl"))
				checker = new OWLPropertyChecker();
			else
				checker = new SKOSPropertyChecker();
			this.badProperties = checker.getProperties("file://" + this.ontology_filename, this.ontology_format_label);
			this._propertiesChecked = true;
		}
		return this.badProperties;
	}
	
	public Map<String,Object> getOldVersionData(){
		return this.oldVersionData;
	}
	
	public void setOldVersionData(Map<String,Object> oldVersionData){
		this.oldVersionData = oldVersionData;
	}
	
	public Set<String> getRemovedClasses() {
		return removedClasses;
	}

	public void setRemovedClasses(Set<String> removedClasses) {
		this.removedClasses = removedClasses;
	}

	public Boolean getBackwardCompatible() {
		return backwardCompatible;
	}

	public Boolean getInCompatible() {
		return inCompatible;
	}

	// Static methods
	/**
	 * Store the ticket as serialized java object.
	 * 
	 * @param filename
	 */
	public static Ticket getTicket(String filename) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(filename));
			Ticket t = (Ticket) ois.readObject();
			ois.close();
			return t;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
}
