package org.gfbio.wrappers;

/**
 * @author Alexander Hinze.Huettl
 */
public class Settings {
	public static String getUploadPath() {
		return "/upload/ontologies/";
	}
	
	public static String getOntologyStorePath(){
		return "/upload/versioning/"; // .../acronym/date/file.owl
	}

	public static String getAdminUsername() {
		return "admin";
	}

	public static String getAdminPassword() {
		return "admin";
	}

	public static String getVirtIP() {
		return "localhost";
//		return "160.45.63.20";
	}

	public static String getVirtUser() {
		return "dba";
//		return "webservice";
	}

	public static String getVirtPassword() {
		return "dba";
//		return "E6:`7k#bzeTu9'W";
	}
}
