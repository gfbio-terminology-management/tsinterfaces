package org.gfbio.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gfbio.wrappers.OntologyInterface;
import org.gfbio.wrappers.Settings;

/**
 * Servlet implementation class EditOntology <br>
 * This class deletes a graph and its metadata when selecting the corresponding uri in
 * the admin view.
 * @author Alexander Hinze-Huettl
 */
public class EditOntology extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Method to delete an ontology and its metadata from virtuoso.
	 * Get parameter: KEY=uri VALUE=ontologies uri in metadata graph with hashed acronym
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(!request.isRequestedSessionIdValid())
			return;
		if(request.getSession(false).getAttribute("currentUser") == null)
			return;

		String uriHash = request.getParameter("uri");
		OntologyInterface o = new OntologyInterface();
		o.deleteResourceFile(uriHash);
		String graph = o.getGraphFromHash(uriHash);
		o.deleteGraph(graph);
		o.deleteMetadata(uriHash);
		this.reindex();
		response.sendRedirect("Administration.jsp?tab=ontologies");
	}

	public void reindex(){
	    try {
            Class.forName("virtuoso.jdbc4.Driver");
            Connection conn = DriverManager.getConnection("jdbc:virtuoso://" + Settings.getVirtIP() + ":1111","dba","dba");
            PreparedStatement st;
            st = conn.prepareStatement("DB.DBA.VT_INC_INDEX_DB_DBA_RDF_OBJ ()");
            st.execute();
            st.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
	}
}
