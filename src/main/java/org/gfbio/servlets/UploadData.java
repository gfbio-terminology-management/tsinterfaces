package org.gfbio.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.gfbio.terminologyinfoextraction.InfoExtractor;
import org.gfbio.wrappers.Settings;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.UnloadableImportException;
import org.semanticweb.skos.SKOSCreationException;

/**
 * Servlet implementation class UploadData. <br>
 * This class represents the upload of an ontology by a user. After some
 * pre-checks, the user is forwarded to Metadata.jsp to update the Metadata.
 * 
 * @author Alexander Hinze-Huettl
 * @author Alexandra La Fleur
 */
public class UploadData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static String hostURL = "jdbc:virtuoso://" + Settings.getVirtIP()
			+ ":1111";
	private static String userName = Settings.getVirtUser();
	private static String password = Settings.getVirtPassword();

	/**
	 * Get requests are not allowed, forward user back to Upload.jsp
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("Upload.jsp");
	}

	/**
	 * Method to upload an ontology and extracted metadata. After ontology was
	 * uploaded and metadata extraction was successful, user will be forwarted
	 * to Metadata.jsp.
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String pathToSave = Settings.getUploadPath();
		ServletFileUpload s = new ServletFileUpload(new DiskFileItemFactory());
		String localFileName = null;

		// TODO check if upload parameters are given
		try {
			List<FileItem> fileList = s.parseRequest(request);

			if (!fileList.isEmpty()) {
				FileItem f = fileList.get(0);
				localFileName = getAvailableFileName(pathToSave + f.getName());

				// save file
				FileOutputStream fileStream = new FileOutputStream(
						localFileName);
				byte[] fileContent = new byte[(int) f.getSize()];
				f.getInputStream().read(fileContent);
				fileStream.write(fileContent);
				fileStream.close();

			} else {
				throw new RuntimeException("No file given"); // TODO handle this
																// error
			}
		} catch (FileUploadException e) {
			e.printStackTrace();
			return;
		}

		// Store local filename into session
		HttpSession session = request.getSession(true);
		if (!session.isNew()) {
			Enumeration<String> attributeNames = session.getAttributeNames();
			while (attributeNames.hasMoreElements()) {
				String attr = attributeNames.nextElement();
				if (!attr.equals("currentUser")){
					session.removeAttribute(attr);
				}
			}
		}
		session.setAttribute("filename", localFileName);
		// Session will be deleted after 10 minutes
		// User has 10 minutes to submit all metadata
		session.setMaxInactiveInterval(60 * 10);
		// Calculate metadata
		try {
			putMetadataIntoRequest(localFileName, session);
			response.sendRedirect("Metadata.jsp");
		} catch (UnloadableImportException e) {
			session.setAttribute("error", e);
			response.sendRedirect("Upload.jsp");
		} catch (OWLOntologyCreationException e) {
			session.setAttribute("error", e);
			response.sendRedirect("Upload.jsp");
		} catch (SKOSCreationException e) {
			session.setAttribute("error", e);
			response.sendRedirect("Upload.jsp");
		}catch (Exception e){
			session.setAttribute("error", e);
			response.sendRedirect("Upload.jsp");
		}
		// Forward user to Metadata.jsp to correct and complete metadata

	}

	/**
	 * This method will extract metadata from the given ontology file by
	 * InfoEtractor. All extracted informations will be stored in given session
	 * as attributes
	 * 
	 * @param filename
	 *            local path to the ontology
	 * @param session
	 *            session where extracted metadata will be stored as attributes
	 */
	private void putMetadataIntoRequest(String filename, HttpSession session)
			throws UnloadableImportException, OWLOntologyCreationException,
			SKOSCreationException, Exception {
		InfoExtractor ex = new InfoExtractor("file:///" + filename,
				this.hostURL, this.userName, this.password);

		HashMap<String, String> map = ex.extractInfos();
		if (map.containsKey("uri") && !map.get("uri").isEmpty()) {
			session.setAttribute("ontology_uri", map.get("uri"));
		}

		if (map.containsKey("titles") && !map.get("titles").isEmpty())
			session.setAttribute("ontology_name", map.get("titles"));

		if (map.containsKey("description") && !map.get("description").isEmpty())
			session.setAttribute("ontology_description", map.get("description"));

		if (map.containsKey("contributors")
				&& !map.get("contributors").isEmpty()) {
			session.setAttribute("ontology_contributors",
					map.get("contributors"));
		}

		if (map.containsKey("creators") && !map.get("creators").isEmpty()) {
			session.setAttribute("ontology_creators", map.get("creators"));
		}

		if (map.containsKey("language") && !map.get("language").isEmpty())
			session.setAttribute("ontology_language", map.get("language"));

		if (map.containsKey("date_modified")
				&& !map.get("date_modified").isEmpty()) {
			session.setAttribute("ontology_release_date",
					map.get("date_modified"));
		}

		if (map.containsKey("date_created")
				&& !map.get("date_created").isEmpty()) {
			session.setAttribute("ontology_creation_date",
					map.get("date_created"));
		}

 		if (map.containsKey("ontoLang") && !map.get("ontoLang").isEmpty()) {
			session.setAttribute("ontology_format_language",
					map.get("ontoLang"));
		}

		if (map.containsKey("labels") && !map.get("labels").isEmpty()) {
			System.out.println("UploadData.java: " + map.get("labels"));
			session.setAttribute("ontology_format_label", map.get("labels")
					.trim());
		}
		if (map.containsKey("synonyms") && !map.get("synonyms").isEmpty()) {
			session.setAttribute("ontology_format_synonym", map.get("synonyms"));
		}
		if (map.containsKey("definitions") && !map.get("definitions").isEmpty()) {
			session.setAttribute("ontology_format_definition",
					map.get("definitions").trim());
		}

		if (map.containsKey("domains") && !map.get("domains").isEmpty()) {
			session.setAttribute("domains", map.get("domains").trim());
		}
		if (map.containsKey("formalityLevel")
				&& !map.get("formalityLevel").isEmpty()) {
			session.setAttribute("formalityLevel", map.get("formalityLevel")
					.trim());
		}
		if (map.containsKey("acronym") && !map.get("acronym").isEmpty()) {
			session.setAttribute("acronym", map.get("acronym").trim());
		}
		if (map.containsKey("version") && !map.get("version").isEmpty()) {
			session.setAttribute("version", map.get("version").trim());
		}
		if (map.containsKey("keywords") && !map.get("keywords").isEmpty()) {
			session.setAttribute("keywords", map.get("keywords"));
		}
		if (map.containsKey("documentation")
				&& !map.get("documentation").isEmpty()) {
			session.setAttribute("documentation", map.get("documentation"));
		}
		if (map.containsKey("homepage") && !map.get("homepage").isEmpty()) {
			session.setAttribute("homepage", map.get("homepage"));
		}
		if (map.containsKey("homepage") && !map.get("homepage").isEmpty()) {
			session.setAttribute("homepage", map.get("homepage"));
		}
		if (map.containsKey("contact_firstname")
				&& !map.get("contact_firstname").isEmpty()) {
			session.setAttribute("contact_firstname", map.get("contact_firstname"));
		}
		if (map.containsKey("contact_lastname")
				&& !map.get("contact_lastname").isEmpty()) {
			session.setAttribute("contact_lastname", map.get("contact_lastname"));
		}
		if (map.containsKey("contact_mail")
				&& !map.get("contact_mail").isEmpty()) {
			session.setAttribute("contact_mail", map.get("contact_mail"));
		}
	}

	/**
	 * If a file with the given filname already exists, this method will
	 * generate an other path which does not exist.
	 * 
	 * Example: path = "/upload/ontologies/foo.owl" if path already exists
	 * getAvailableFileName(path) will return "/upload/ontologies/foo(0).owl"
	 * 
	 * @param fileName
	 *            path to check
	 */
	String getAvailableFileName(String fileName) {
		String extension = FilenameUtils.getExtension(fileName);
		String filebase = FilenameUtils.getBaseName(fileName);
		String path = FilenameUtils.getFullPath(fileName);
		String availableFileName = path + filebase + "." + extension;

		File file = new File(availableFileName);

		for (int i = 0; file.exists(); i++) {
			availableFileName = path + filebase + "(" + String.valueOf(i) + ")"
					+ "." + extension;
			file = new File(availableFileName);
		}
		return availableFileName;
	}
}
