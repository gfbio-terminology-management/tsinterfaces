package org.gfbio.servlets;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.gfbio.enums.Ontology;
import org.gfbio.metadata.GFBioMetadataWriter;
import org.gfbio.upload.GFBioOntologyUploader;
import org.gfbio.wrappers.Settings;
import org.gfbio.wrappers.Ticket;
import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.HermiT.datatypes.MalformedLiteralException;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.reasoner.OWLReasoner;

/**
 * Servlet implementation class EditTicket <br>
 * This class handles both actions from a user and an admin.
 * When the user submits the data entered in Metadata.jsp, the request is
 * stored in a ticket to be handled by the admin separately. <br>
 * When the admin logs in and edits or inspects the Metadata entered by the user,
 * the ticket information is extracted and concretly uploaded into a Virtuoso graph.
 * @author Alexander Hinze-Huettl 
 * @author Alexandra La Fleur
 */
public class EditTicket extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * This methods handles requests to create, edit and remove tickets. It can
	 * be used by normal users to create a ticket after uploading an ontology or
	 * used by admins to edit and remove tickets respectively uploading a ticket
	 * and its ontology to virtuoso
	 * 
	 * For admins only, this requires an other post parameter "ticket_location"
	 * which defines the local path to the ticket post parameter 'action' needs
	 * to be one of these values: -upload: upload an ontology and its ticket
	 * fields to virtuoso -save: save changes, after admin edited a ticket
	 * -delete: remove a ticket from the file system
	 * 
	 * For all users, if post parameter "upload" is set: -submit: create a new
	 * ticket, ticket values will be read by post parameters (take a look in
	 * gfbio.wrappers.Ticket)
	 */
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// If these is no valid ID, the user neither uploaded an ontology, nor
		// an admin handles a ticket
		if (!request.isRequestedSessionIdValid())
			return;

		/*
		 * The session attribute "currentUser" was set in servlet Login.java
		 * after successful login. It is required to perform admin operations
		 * like: edit, remove & upload ticket.
		 * 
		 * The session attribute "filename" will be set after uploading an
		 * ontology in servlet UploadData.java. It contains the local path to
		 * the uploaded ontology file. This is necessary to create new tickets.
		 */
		boolean isAdmin = request.getSession(false).getAttribute("currentUser") != null;
		boolean isUser = request.getSession(false).getAttribute("filename") != null;

		String type;

 		if ((type = request.getParameter("action")) != null && isAdmin) {
 			 			
			// Handle admin operations
			String ticketLocation = request.getParameter("ticket_location");
			if (ticketLocation == null)
				return;
			Ticket ticket = Ticket.getTicket(ticketLocation);

			if (ticket == null)
				return;
			if (type.equals("upload")) {
				ticket.update(request);
				
				// save the file on the server (or locally for the moment)
				uploadTicket(ticket);
				File source = new File(ticket.getOntologyFileLocation());
				File dest = new File(ticket.getOntologyVersoningLocation());
				try {
					FileUtils.copyFile(source, dest);
				} catch (IOException e) {
					e.printStackTrace();
				}

				ticket.deleteFiles();
				response.sendRedirect("Administration.jsp?tab=tickets");
			} else if (type.equals("save")) {
				// update & save changed ticket
				ticket.update(request);
				response.sendRedirect("Administration.jsp?tab=tickets");
			} else if (type.equals("delete")) {
				// delete ticket and its ontology file
				ticket.deleteFiles();
				response.sendRedirect("Administration.jsp?tab=tickets");
			}
		} else if ((type = request.getParameter("upload")) != null && isUser) {
			// Handle user oprations
			if (type.equals("submit")) {
				// create new ticket and save it
				Ticket ticket = new Ticket(request);
				HttpSession session = request.getSession(true);
				try {
					isConsistent(ticket.getOntologyFileLocation());
					GFBioMetadataWriter w = new GFBioMetadataWriter(null);
					Map<String, Object> oldOntoData = w.getOldOntoData(ticket.getOntologyAcronym());
					ticket.setOldVersionData(oldOntoData);
					Set<String> removedClasses = w.checkConcepts(ticket.getOntologyAcronym().hashCode(), "file://"+ticket.getOntologyFileLocation());
					ticket.setRemovedClasses(removedClasses);
				} catch (OWLOntologyCreationException e) {
//					session.setAttribute("error", e);
//					response.sendRedirect("Metadata.jsp");
//					return;
				}
				catch (MalformedLiteralException e){
//					session.setAttribute("error", e);
//					response.sendRedirect("Metadata.jsp");
//					return;
				}
				ticket.save(ticket.getOntologyFileLocation() + "_ticket.ser");

				// Create future links where the ontology will be available
				// after admin checked it
				String futurOntologyLink = "http://terminologies.gfbio.org/api/terminologies/"
						+ ticket.getOntologyAcronym();
				String futureMetrics = futurOntologyLink + "/metrics";
				// forward user
				session.setAttribute("futurOntologyLink", futurOntologyLink);
				session.setAttribute("futureMetrics", futureMetrics);
				response.sendRedirect("SuccessfulUpload.jsp");
			}
		}
	}

	private boolean isConsistent(String ontologyFileLocation)
			throws OWLOntologyCreationException, MalformedLiteralException {
		boolean consistent = false;
		try {
			OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
			IRI iri = IRI.create("file://" + ontologyFileLocation);
			OWLOntology o = manager.loadOntologyFromOntologyDocument(iri);
			OWLReasoner reasoner = new Reasoner.ReasonerFactory().createReasoner(o);
			consistent = reasoner.isConsistent();
			if (!consistent){
				throw new OWLOntologyCreationException("This ontology is not consistent.");
			}
		}
		catch (MalformedLiteralException e){
			throw e;
		}
		
		return consistent;
	}

	/**
	 * Convert a date into a string formated by MM/dd/yy
	 * 
	 * @param d
	 *            Date to convert
	 * @return string representation of the given date in MM/dd/yy format
	 */
	private String dateToString(Date d) {
		if (d != null) {
			return (new SimpleDateFormat("yyyy-MM-dd")).format(d);
		} else {
			return "";
		}
	}

	private String arrayToString(String[] array) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length - 1; i++) {
			sb.append(array[i]);
			sb.append(",");
		}
		sb.append(array[array.length - 1]);
		return sb.toString();
	}

	/**
	 * Upload an ontology to virtuoso by its ticket
	 * 
	 * @param t
	 *            Ticket which describes the ontology
	 */
	private void uploadTicket(Ticket t) {
		GFBioMetadataWriter ontologyWriter = new GFBioMetadataWriter("file://"
				+ t.getOntologyFileLocation());
		HashMap<Ontology, String> metadata = new HashMap<Ontology, String>();

		metadata.put(Ontology.name, t.getOntologyName());
		metadata.put(Ontology.uri, t.getOntologyUri());
		metadata.put(Ontology.description, t.getOntologyDescription().replace("\n", " "));
		metadata.put(Ontology.acronym, t.getOntologyAcronym());

		metadata.put(Ontology.version, t.getOntologyVersion());
		metadata.put(Ontology.status, t.getOntologyStatus());

		metadata.put(Ontology.domains, arrayToString(t.getOntologyDomains()));

		metadata.put(Ontology.creation_date,
				dateToString(t.getOntologyCreationDate()));
		metadata.put(Ontology.release_date,
				dateToString(t.getOntologyRealeaseDate()));

		metadata.put(Ontology.release_date,
				dateToString(t.getOntologyRealeaseDate()));
		metadata.put(Ontology.keywords, arrayToString(t.getOntologyKeywords()));

		metadata.put(Ontology.naturalLanguage, t.getOntologyLanguage());

		metadata.put(Ontology.creators, arrayToString(t.getOntologyCreators()));
		metadata.put(Ontology.contributors,
				arrayToString(t.getOntologyContributors()));

		// Format & Ontologylan stuff
		metadata.put(Ontology.label, t.getOntologyFormatLabel());
		metadata.put(Ontology.synonym, t.getOntologyFormatSynonym());
		metadata.put(Ontology.definition, t.getOntologyFormatDefinition());

		metadata.put(Ontology.ontologyLanguage, t.getOntologyFormatLanguage());
		metadata.put(Ontology.formality_level,
				t.getOntologyFormatFormalitLevel());

		// Contact information
		metadata.put(Ontology.contact_firstname, t.getOntologyContactFirstName());
		metadata.put(Ontology.contact_lastname, t.getOntologyContactLastName());
		metadata.put(Ontology.contact_email, t.getOntologyContactEmail());
		metadata.put(Ontology.homepage, t.getOntologyHomepage());
		metadata.put(Ontology.documentation, t.getOntologyDocumentation());

		metadata.put(Ontology.ontology_store_location,
				t.getOntologyVersoningLocation());
		metadata.put(Ontology.ontology_file, t.getOntologyFileLocation());
		metadata.put(Ontology.backwardCompatibility, String.valueOf(t.getBackwardCompatible()));
		metadata.put(Ontology.inCompatibility, String.valueOf(t.getInCompatible()));
		
		HashMap<String, Object> metricsMap = new HashMap<String, Object>();
		metricsMap.put("numberOfClasses", t.getOntologyNumberOfClasses());
		metricsMap.put("numberOfIndividuals",
				t.getOntologyNumberOfIndividuals());
		metricsMap.put("maximumNumberOfChildren",
				t.getOntologyMaximumNumberOfChildren());
		metricsMap.put("maximumDepth", t.getOntologyMaximumDepth());
		metricsMap.put("classeswithMoreThan25Children",
				t.getOntologyClassesMore25Children());
		metricsMap.put("averageNumberOfChildren",
				t.getOntologyAverageNumberOfChildren());
		metricsMap.put("numberOfProperties", t.getOntologyNumberOfProperties());
		metricsMap.put("numberOfClassesWithASingleChild",
				t.getOntologyClassesSingleChild());
		metricsMap.put("numberOfClassesWithoutDefinition",
				t.getOntologyClassesWithoutDefinition());
		metricsMap.put("numberOfLeaves", t.getOntologyNumberOfLeaves());
		metricsMap.put("classesWithoutLabel",
				t.getOntologyClassesWithoutLabel());
		metricsMap.put("classesWithMoreThan1Parent",
				t.getOntologyClassesWithMoreThan1Parent());
		metricsMap.put("descriptionLogicName", t.getDlExpressivity());
		System.out.println(t);
		System.out.println(t.getOntologyNumberOfClasses());

		
		ontologyWriter.insert(metadata, metricsMap);
		GFBioOntologyUploader uploader = new GFBioOntologyUploader(
				Settings.getVirtIP() + ":1111", Settings.getVirtUser(),
				Settings.getVirtPassword());

		/**
		 * if you get a 'no access' exception, please read this:
		 * https://gitlab.com
		 * /gfbio-terminology-management/tsinterfaces/wikis/upload_directorybb
		 */
		uploader.upload(t.getOntologyFileLocation(), t.getOntologyAcronym());
		uploader.index();
	}

}
