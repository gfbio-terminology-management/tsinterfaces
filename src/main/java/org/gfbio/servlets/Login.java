package org.gfbio.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.gfbio.wrappers.Settings;

/**
 * Servlet to handle logins
 * @author Alexander Hinze-Huettl
 */
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * Get requests are not allowed, forward user back to AdminLogin.jsp
	 */
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.sendRedirect("AdminLogin.jsp");
    }

	/**
	 * Method to login for admins.
	 * Username and password must be set in post parameters 'username' and 'password'.
	 * If login was successful a session 'currentUser' will be set and user will be forwarded to Administration.jsp.
	 * Otherwise user will be forwarded back to AdminLogin.jsp
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		if(username.equals(Settings.getAdminUsername()) && password.equals(Settings.getAdminPassword())){
			HttpSession session = request.getSession(true);
			session.setAttribute("currentUser", username);
			response.sendRedirect("Administration.jsp");
		}else{
			response.sendRedirect("AdminLogin.jsp");
		}
	}
}
