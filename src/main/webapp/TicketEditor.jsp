<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="java.io.File"%>
    <%@ page import="java.util.Date" %>
    <%@ page import="java.text.SimpleDateFormat"%>
    <%@ page import="org.gfbio.wrappers.Ticket"%>
    <%@ page import="org.gfbio.wrappers.OmvInfo"%>
	<%@page import="org.gfbio.wrappers.OmvInfo.OmvIndividual"%>
	<%@ page import="org.gfbio.wrappers.checker.*"%>
	<%@ page import="java.util.Map" %>	
    <%@ page import="java.util.List"%>
	<%@ page import="java.util.Map.Entry" %>	
	<%@page import="java.util.ArrayList"%>

<%!
OmvInfo oi = new OmvInfo();
String concatStringArray(String array[]){
	if(array.length == 0)
		return "";
	String result = array[0];
	for(int i = 1; i < array.length; i++)
		result += "," + array[i];
	return result;
}
String dateToString(Date date){
	if(date == null)
		return "";
	return (new SimpleDateFormat("MM/dd/yyyy")).format(date);
}
String arrayToJSArray(String[] array){
	List<String> tmp = new ArrayList<String>();
	for(String s : array)
		tmp.add(s);
	return arrayToJSArray(tmp);
}

String arrayToJSArray(List<String> array){
	if(array.size() == 0)
		return "[]";
	if(array.size() == 1)
		return "[\"" + array.get(0) + "\"]";

	StringBuilder s = new StringBuilder();
	s.append("[\"");
	s.append(array.get(0));
	s.append("\"");
	for(int i = 1; i < array.size(); i++){
		s.append(',');
		s.append("\"");
		s.append(array.get(i).trim());
		s.append("\"");
	}
	s.append("]");
	return s.toString();
}
%>

<%
Ticket t;
Map<String,PropChecker.PropertyStatus> badProperties;
if(session.getAttribute("currentUser") == null){
	response.sendRedirect("AdminLogin.jsp"); //Send user back
	return;
}else{
	String ticketLocation = (String)request.getParameter("location");
	if(! new File(ticketLocation).exists()){
		response.sendRedirect("AdminLogin.jsp");
		return;
	}
	t = Ticket.getTicket(ticketLocation);
	if(!t.metricsCalculated()){
		t.calculateMetrics();
		t.save(t.getTicketFileLocation());
	}
	badProperties = t.getBadProperties();
}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="scripts/jquery.tagit.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="scripts/tag-it.js"></script>

<script>

	function askForDelete(){
		return confirm("Are you sure you want to delete this file ?");
	};
	
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	}
	
	var domains = [];

	var keywords = [];
	<%
	for (OmvIndividual domain : oi.getOntologyDomains()){
		String domainURI = domain.getUri();
		String domainName = domain.getLabel();
		List<String> keywords = oi.getOntologyDomainKeywords(domainURI);
		out.println("keywords['" + domainName + "'] = " + arrayToJSArray(keywords) + ";");
		out.println("domains['" + domainName + "'] = '" + domainURI + "';");
	}
   %>
	
	function validateForm(){
		var email = $("#ontology_contact_email").val();
		console.log(email);
		if(!validateEmail(email)){
			alert("please choose a valid email address");
			return false;
		}
		
		//Convert domainnames to domainuris
		//into 
		var allUris = '';
		var selectedDomains = $('#domains_tags').tagit('assignedTags');
		for(var i = 0; i < selectedDomains.length -1; i++){
			allUris +=  domains[selectedDomains[i]] + ",";
		}
		allUris += domains[selectedDomains[selectedDomains.length -1]];
		$('#ontology_domains').val(allUris);
		console.log($('#ontology_domains').val());
		return true;
	}

	function createAutocompleteDropdown(inputID, data, tooltipText){
		var jID = "#" + inputID;
		$(jID).autocomplete({source : data, minLength: 0, delay: 0,});
		var visible = false;
		var button = $( "<a>" ).attr( "tabIndex", -1 )
							   .attr( "title", tooltipText)
							   .tooltip()
							   .button({
									icons: {primary: "ui-icon-triangle-1-s"},text: false
							   })
							   .mousedown(function() {
									visible = ($(jID).autocomplete( "widget" ).is( ":visible" ));
							   })
							   .click(function(){
								   $(jID).focus();
								   if (visible)
									   return;
								   $(jID).autocomplete("search","");
							   });
		if(data.length > 0)
			$(jID).val(data[0]);
		button.insertAfter($(jID));
	};
	
	
	$(document).ready(function(){
		$("#tabs").tabs();
		
		$("#ontology_creation_date").datepicker();
		$("#ontology_release_date").datepicker();

		//Prepare widgets to select domains
		$('#domains_tags').tagit({
			singleField: true,
			readOnly : true,
		});
		var ontoDomains = <%= arrayToJSArray(t.getOntologyDomains())%>;
		for(var i = 0; i < ontoDomains.length; i++){
			for(var key in domains){
				console.log(key);
				console.log(domains[key]);
				console.log(ontoDomains[i]);
				if(domains[key] == ontoDomains[i]){
					console.log("add");
					$('#domains_tags').tagit('createTag',key);
				}
			}
		}
		$('#ontology_domain_dropdown').change(function (){
			var selectedDomainName = this.value;
			$('#domains_tags').tagit('createTag',selectedDomainName);
		});
		for(var key in domains){
			$('#ontology_domain_dropdown').append($('<option>', { 
		        value: key,
		        text : key 
		    }));
		}

		
		$("#ontology_keywords").val((<%= arrayToJSArray(t.getOntologyKeywords()) %>).join(","));
		$("#ontology_creators").val((<%= arrayToJSArray(t.getOntologyCreators()) %>).join(","));
		$("#ontology_contributors").val((<%= arrayToJSArray(t.getOntologyContributors()) %>).join(","));
		$('#keywords_tags').tagit({
			singleField: true,
            singleFieldNode: $('#ontology_keywords'),
		});
		$('#creators_tags').tagit({
			singleField: true,
            singleFieldNode: $('#ontology_creators'),
            
		});
		$('#contributors_tags').tagit({
			singleField: true,
            singleFieldNode: $('#ontology_contributors'),
		});


		$('#upload').button();
		$('#save').button();
		$('#delete').button();
		$('#cancel').button();

		var formatName = <%= arrayToJSArray(t.getOntologyFormatLabel().split(","))%>;
		var formatSynonym = <%= arrayToJSArray(t.getOntologyFormatSynonym().split(","))%>;
		var formatDefinition = <%= arrayToJSArray(t.getOntologyFormatDefinition().split(","))%>;

		createAutocompleteDropdown("ontology_format_label",formatName);
		createAutocompleteDropdown("ontology_format_synonym",formatSynonym);
		createAutocompleteDropdown("ontology_format_definition",formatDefinition);

		$("#cancel").click(function(){
			window.location	= window.location.href.replace("TicketEditor","Administration");
		});
		
		$("#compatTab").click(function(){
			$("#tabs").tabs({active:3}); 
			return false;
		});
		
	});
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>edit Ticket</title>
</head>
<body>
<div id="tabs">
<ul>
	<li><a href="#tabs-1">Metadata</a></li>
	<li><a href="#tabs-2">Metrics</a></li>
	<% if(!badProperties.isEmpty()) { %>
		<li><a href="#tabs-3">Bad properties (<%=badProperties.size()%>) </a></li> 
	<%} %>
	<% if(t.getOldVersionData() != null && t.getOldVersionData().size()>0) { %>
		<li><a href="#tabs-4">Compatibility</a></li>
	<%} %>
</ul>

<div id="tabs-1">
<form  action="EditTicket" method="POST" onsubmit="return validateForm()">
	<input type="hidden" name="ticket_location" value="<%=t.getTicketFileLocation()%>">
	<fieldset style="width: 60%">
		<table border="0" cellpadding="0" cellspacing="4">
			<tr>
				<td>Name:</td>
				<td style="padding-left: 70px;"> <input type="text" name="ontology_name" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyName()%>"></td>
			</tr>
			<tr>
				<td>URI:</td> 
				<td style="padding-left: 70px;"><input type="text" name="ontology_uri" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyUri()%>"></td>
			</tr>
			<tr>
				<td>Description:</td>
				<td style="padding-left: 70px;"><textarea class="FormElement"  name="ontology_description" style="width: 498px;" rows="4"><%=t.getOntologyDescription()%></textarea></td>
			</tr>
			<tr><td></td></tr>
			<tr>
				<td>Acronym:</td>
				<td style="padding-left: 70px;"><input type="text" name="ontology_acronym" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyAcronym()%>"></td>
			</tr>	
			
		    <tr>
		    	<td>Version:</td>
		    	<td style="padding-left: 70px;"><input type="text" name="ontology_version" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyVersion() %>"></td>
		    </tr>
		    <tr>
		    	<td>Status:</td>
		    	<td style="padding-left: 70px;"><select style="width:120px;" class="ui-widget-content ui-corner-all" name="ontology_status">
		    	<%
					String[] status = new String[] {"Alpha", "Beta", "Release", "Production"};
		    		for(String s : status){
		    			if (s.equals(t.getOntologyStatus())){
		    				out.println("<option selected=\"selected\">" + t.getOntologyStatus() + "</option>");	
		    			}
		    			else if(!s.equals(t.getOntologyStatus())){
		    				out.println("<option>" + s + "</option>");
		    			}
		    		}
		    	%>
					</select></td>
			</tr>
			<tr>
				<td>Domain:</td>
				<td style="padding-left: 70px;">
						<select style="width:120px;" class="ui-widget-content ui-corner-all"  id="ontology_domain_dropdown">
							<option></option>
						</select>
						<ul id="domains_tags"></ul>
                   		<input type="hidden" name="ontology_domains" id="ontology_domains">				
				    </td>
			</tr>
			<tr><td><br></td></tr>
			<tr>
				<td>Creation date:</td>
				<td style="padding-left: 70px;"><input name="ontology_creation_date" size="50" id="ontology_creation_date" class="ui-widget-content ui-corner-all" type="text" value="<%=dateToString(t.getOntologyCreationDate())%>"></td>
			</tr>
			<tr>
				<td>Release date:</td>
				<td style="padding-left: 70px;"><input name="ontology_release_date" size="50" id="ontology_release_date" class="ui-widget-content ui-corner-all" type="text" value="<%=dateToString(t.getOntologyRealeaseDate())%>"></td>
			</tr>
			<tr><td></td></tr>
			<tr>
				<td align="left">Keywords:</td>
				<td style="padding-left: 70px;">
				<ul id="keywords_tags"></ul>
				<input type="hidden" name="ontology_keywords" id="ontology_keywords">
				<br><font size="1">(separated by comma)</font></td></tr>
			<tr>
				<td>Language:</td>
				<td style="padding-left: 70px;"><input type="text" name="ontology_language" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyLanguage()%>"></td>
			</tr>
			<tr>
				<td align="left">Creators:</td>
				<td style="padding-left: 70px;">
				<ul id="creators_tags"></ul>
				<input type="hidden" name="ontology_creators" id="ontology_creators"></input><font size="1">(separated by comma)</font></td></tr>
			<tr>
				<td align="left">Contributors: </td>
				<td style="padding-left: 70px;">
				<ul id="contributors_tags"></ul>
				<input type="hidden" name="ontology_contributors" id="ontology_contributors"><br><font size="1">(separated by comma)</font></td>
			</tr>				
			</table>
			</fieldset>
			<br>
			<fieldset style="width: 60%">
				<legend>Format</legend>
				<table border="0" cellpadding="0" cellspacing="4">
					<tr>
						<td>Name property:</td>
						<td style="padding-left: 20px;"><input name="ontology_format_label" size="50" class="ui-widget-content ui-corner-all" id="ontology_format_label"></td>
					</tr>
					<tr>
						<td>Synonym property:</td>
						<td style="padding-left: 20px;"><input name="ontology_format_synonym" size="50" class="ui-widget-content ui-corner-all" id="ontology_format_synonym"></td>
					</tr>
					<tr>
						<td>Definition property:</td>
						<td style="padding-left: 20px;"><input name="ontology_format_definition" size="50" class="ui-widget-content ui-corner-all" id="ontology_format_definition"></td>
					</tr>
					<tr>
						<td>Ontology Language:</td>
						<td style="padding-left: 20px;"><select style="width:120px;" class="ui-widget-content ui-corner-all" name="ontology_format_language">
						<%	
						String owl = "http://omv.ontoware.org/ontology#OWL";
						String skos = "http://purl.org/gfbio/metadata-schema#SKOS";
						
						String ontologyFormatLanguage = t.getOntologyFormatLanguage();
						if (owl.equals(ontologyFormatLanguage)){
							out.println("<option value=\"" + ontologyFormatLanguage + "\">OWL</option>");
							out.println("<option value=\"" + skos + "\">SKOS</option>");
						}
						else {
							out.println("<option value=\"" + skos + "\">SKOS</option>");
							out.println("<option value=\"" + ontologyFormatLanguage + "\">OWL</option>");
						}
						%>
						</select></td>
					</tr>
					<tr>
						<td>Formality level:</td>
						<td style="padding-left: 20px;"><select style="width:120px;" class="ui-widget-content ui-corner-all" name="ontology_format_formality_level">
					    <%
					    String formalityLevel = t.getOntologyFormatFormalitLevel();
						List<OmvIndividual> formalityLevels = oi.getOntologyFormalityLevels();
						if (formalityLevel.length()>0){
							
							for(OmvIndividual level : formalityLevels){
								if (level.getUri().equals(formalityLevel)){
									out.println("<option value=\"" + formalityLevel + "\" selected=\"selected\">" + level.getLabel() + "</option>");
								}
								else if (!level.getUri().equals(formalityLevel)){
									out.println("<option value=\"" + level.getUri() + "\">" + level.getLabel() + "</option>");
								}
							}
						}
						%>
					    </select></td>
					</tr>
				</table>
			</fieldset>
			<br>			
			<fieldset style="width: 60%">
				<legend>Contact</legend>
				<table border="0" cellpadding="0" cellspacing="4">
					<tr>
						<td>First Name:</td>
						<td style="padding-left:6px;"><input type="text" name="ontology_contact_firstname" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyContactFirstName()%>"></td>
					</tr>
					<tr>
						<td>Last Name:</td>
						<td style="padding-left:6px;"><input type="text" name="ontology_contact_lastname" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyContactLastName()%>"></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td style="padding-left:6px;"><input type="text" id="ontology_contact_email" name="ontology_contact_email" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyContactEmail()%>"></td>
					</tr>
					<tr>
						<td>Home page:</td>
						<td style="padding-left:6px;"><input type="text" name="ontology_homepage" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyHomepage()%>"></td>
					</tr>
					<tr>
						<td>Documentation page:</td>
						<td style="padding-left:6px;"><input type="text" name="ontology_documentation" size="50" class="ui-widget-content ui-corner-all" value="<%=t.getOntologyDocumentation()%>"></td>
					</tr>
					<tr><td></td></tr>
				</table>
		    </fieldset>
			<br>
			<%if (t.getOldVersionData() != null && t.getOldVersionData().size()>0) { %>
			<fieldset style="width: 60%">
				<legend>Compatibility</legend>				
				There exists an old version of the uploaded ontology in the graph.
				<br>
				Please check the tab <u><a id="compatTab" href="">Compatibility</a></u>, and decide whether this version is <font color="red">backward compatible</font> or <font color="red">incompatible</font> with the old version! 
				<br>
				<br>
				<input type="radio" name="compatibility" value="backwardCompatible" checked="checked">Backward Compatible?  
				<br>
				<input type="radio" name="compatibility" value="incompatible">Incompatible?
				<br>
			</fieldset>
			<% } %>
			
			<input type="submit" name="action" value="upload" id="upload">
			<input type="submit" name="action" value="save" id="save">			
</form>
<form action="EditTicket" method="POST" onsubmit="return askForDelete()" >
	<input type="hidden" name="ticket_location" value="<%=t.getTicketFileLocation()%>">
	<input type="submit" name="action" value="delete" id="delete">
</form>
	
	<input type="submit" name="action" value="cancel" id="cancel">
</div>
<div id="tabs-2" >
	<h2>Metrics</h2>
	<table  class="ui-responsive ui-shadow" >
		<thead>
			<tr><th>metric</th><th>value</th></tr>
		</thead>
		<tbody>
			<tr><td>Number of classes</td> <td><%=t.getOntologyNumberOfClasses()%></td> </tr>
			<tr><td>Number of individuals</td> <td><%=t.getOntologyNumberOfIndividuals()%></td> </tr>
			<tr><td>Number of properties</td> <td><%=t.getOntologyNumberOfProperties()%></td> </tr>
			<tr><td>Maximum depth</td> <td><%=t.getOntologyMaximumDepth()%></td> </tr>
			<tr><td>Maximum number of children</td> <td><%=t.getOntologyMaximumNumberOfChildren()%></td> </tr>
			<tr><td>Average number of children</td> <td><%=t.getOntologyAverageNumberOfChildren()%></td> </tr>
			<tr><td>Classes with a single child</td> <td><%=t.getOntologyClassesSingleChild()%></td> </tr>
			<tr><td>Classes with 25 children and more</td> <td><%=t.getOntologyClassesMore25Children()%></td> </tr>
			<tr><td>Classes without definition</td> <td><%=t.getOntologyClassesWithoutDefinition()%></td> </tr>
			<tr><td>Classes without label</td> <td><%=t.getOntologyClassesWithoutLabel()%></td> </tr>
			<tr><td>Number of leaves</td> <td><%=t.getOntologyNumberOfLeaves()%></td> </tr>
			<tr><td>Classes with more than 1 Parent</td> <td><%=t.getOntologyClassesWithMoreThan1Parent()%></td> </tr>
			<tr><td>DL Expressivity</td><td><%=t.getDlExpressivity()%></td></tr>
		</tbody>
	</table>
</div>
<% if(!badProperties.isEmpty()) { %>
<div id="tabs-3">
	<h2>Bad properties</h2>
			<table class="ui-responsive ui-shadow"  >
					<thead>
						<tr><th>error</th><th>uri</th></tr>
					</thead>
					<tbody>
					 	<%
					 		for (Entry<String, PropChecker.PropertyStatus> e : badProperties
					 				.entrySet()) {
					 			out.println("<tr>");
					 			out.println("<td><b>" + e.getValue() + "</b></td>");
					 			out.println("<td><a href='" + e.getKey() + "'>" + e.getKey() + "</a></td>");
					 			out.println("</tr>");
					 		}
					 	%>
					</tbody>
	</table>
</div>
<%} %>
<% if(t.getOldVersionData() != null && t.getOldVersionData().size()>0) { %>
<div id="tabs-4" >
	<h2>Compatibility</h2>
	<p>There exists at least one prior version of this ontology on the server. 
	<p>The following table displays the differences in calculated metrics.
	<table  class="ui-responsive ui-shadow" >
		<thead>
			<tr><th>metric</th><th>actual version</th><th>existing version</th></tr>
		</thead>
		<tbody>
			<tr><td>Number of classes</td> <td><%=t.getOntologyNumberOfClasses()%></td><td><%=t.getOldVersionData().get("numberOfClasses")%></td></tr>
			<tr><td>Number of individuals</td> <td><%=t.getOntologyNumberOfIndividuals()%></td><td><%=t.getOldVersionData().get("numberOfIndividuals")%></td></tr>
			<tr><td>Number of properties</td> <td><%=t.getOntologyNumberOfProperties()%></td><td><%=t.getOldVersionData().get("numberOfProperties")%></td></tr>
			<tr><td>Maximum depth</td> <td><%=t.getOntologyMaximumDepth()%></td><td><%=t.getOldVersionData().get("maximumDepth")%></td></tr>
			<tr><td>Maximum number of children</td> <td><%=t.getOntologyMaximumNumberOfChildren()%></td><td><%=t.getOldVersionData().get("maximumNumberOfChildren")%></td></tr>
			<tr><td>Average number of children</td> <td><%=t.getOntologyAverageNumberOfChildren()%></td><td><%=t.getOldVersionData().get("averageNumberOfChildren")%></td></tr>
			<tr><td>Classes with a single child</td> <td><%=t.getOntologyClassesSingleChild()%></td><td><%=t.getOldVersionData().get("numberOfClassesWithASingleChild")%></td></tr>
			<tr><td>Classes with 25 children and more</td> <td><%=t.getOntologyClassesMore25Children()%></td><td><%=t.getOldVersionData().get("classeswithMoreThan25Children")%></td></tr>
			<tr><td>Classes without definition</td> <td><%=t.getOntologyClassesWithoutDefinition()%></td><td><%=t.getOldVersionData().get("numberOfClassesWithoutDefinition")%></td></tr>
			<tr><td>Classes without label</td> <td><%=t.getOntologyClassesWithoutLabel()%></td><td><%=t.getOldVersionData().get("classesWithoutLabel")%></td></tr>
			<tr><td>Number of leaves</td> <td><%=t.getOntologyNumberOfLeaves()%></td><td><%=t.getOldVersionData().get("numberOfLeaves")%></td></tr>
			<tr><td>Classes with more than 1 Parent</td> <td><%=t.getOntologyClassesWithMoreThan1Parent()%></td><td><%=t.getOldVersionData().get("classesWithMoreThan1Parent")%></td></tr>
		</tbody>
	</table>
	<br>
	<%if (t.getRemovedClasses().size()>0){ %>
	The following Classes were removed:
	<br>
	<ul>
		<% for (String removedClass : t.getRemovedClasses()) { %>
		<li><%= removedClass %></li>
		<% } %>
	</ul>
	</div>
	<% } else { %>
	No classes were removed from the existing version.
	<% } %>
<%} %>
</div>
</body>
</html>