<%@page import="org.gfbio.wrappers.OmvInfo"%>
<%@page import="org.gfbio.wrappers.OntologyInterface"%>
<%@page import="virtuoso.jena.driver.VirtInfGraph"%>
<%@page import="java.util.Map"%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"
    %>
    <%@ page import="java.io.File"%>
    <%@ page import="org.gfbio.wrappers.Ticket"%>
    <%@ page import="org.gfbio.wrappers.Settings" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="scripts/jquery.tagit.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>

<script>
	$(function() {
		$("#tabs").tabs();
		var ticketToSelect;
		<%
		String param = request.getParameter("tab");
		if(param != null){
			out.println("ticketToSelect = $('#tabs a[href=\"#" + param + "\"]').parent().index();");
			out.println("$('#tabs').tabs({ active: ticketToSelect });");
		}
		%>
		$("a[name='edit']").each(function() {
		    $(this).button();
		})
	});

	function askForDelete(){
		return confirm("Are you sure you want to delete this graph and all depending files ?");
	}
</script>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Adminpanel</title>
</head>
<body>
<%if(session.getAttribute("currentUser") == null){
	response.sendRedirect("AdminLogin.jsp"); //Send user back
}else{%>
	<h1>Welcome <%=session.getAttribute("currentUser").toString()%></h1>
	<div id="tabs">
		<ul>
			<li><a href="#tickets">Tickets</a></li>
			<li><a href="#ontologies">Ontologies</a></li>
		</ul>
		<div id="tickets">
		<%if (new File(Settings.getUploadPath()).listFiles().length > 0) {%>
			<table>
				<thead>
					<tr><th>Acronym</th><th>Name</th><th>Path</th><th></th></tr>
				</thead>
				<tbody>
				 	<%for(File f : new File(Settings.getUploadPath()).listFiles()){
						if(f.getName().endsWith("_ticket.ser")){
							Ticket t = Ticket.getTicket(f.getAbsolutePath());
							out.println("<tr>");
							out.println("<td>" + t.getOntologyAcronym() + "</td>");
							out.println("<td>" + t.getOntologyName() + "</td>");
							out.println("<td>" + t.getOntologyFileLocation() + "</td>");
							out.println("<td><a href='TicketEditor.jsp?location=" + t.getTicketFileLocation() + "' name='edit'>open</a></td>");
							out.println("</tr>");
						}
				 	}%>
				</tbody>
			</table>
		<% } else {%> There are currently no tickets uploaded.<%}%>	
		</div>
		<div id="ontologies">
		<%if (new OntologyInterface().listOntologies().size()>0) {%>
			<table>
				<thead>
					<tr><th>Acronym</th><th>Metadata Uri</th><th></th></tr>
				</thead>
				<tbody>
				 	<%
				 	OntologyInterface o = new OntologyInterface();
				 	Map<String,String> m = o.listOntologies();
				 	for(Map.Entry<String,String> onto : m.entrySet()){
				 		out.println("<tr>");
				 		out.println("<td>" + onto.getValue() + "</td>");
				 		out.println("<td>" + onto.getKey() + "</td>");
						out.println("<td><a href='EditOntology?uri=" +  onto.getKey()  + "' onclick='return askForDelete()' name='edit'>delete</a></td>");
				 		out.println("</tr>");
				 	}
				 	%>
				</tbody>
			</table>
			<%} else { %> There are currently no ontologies uploaded.<%}%>	 
		</div>
		
	</div>
<%}%>
</body>
