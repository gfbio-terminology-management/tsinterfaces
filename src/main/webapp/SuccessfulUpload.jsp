<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Successful Upload</title>
</head>
<body>
	<h2>Ontology uploaded successfully</h2>
	<p>Thank you for submitting your ontology</p>
	<br>
	<p>We will now put you ontology on the queue to be processed. Please keep in mind that it may take up to serveral hours before users
	will be able to explore and search your ontology.</p>
	<br>
	<p>When your ontology is ready for viewing, it will be availible here:</p>
	<a href="<%=session.getAttribute("futurOntologyLink").toString() %>"><%=session.getAttribute("futurOntologyLink").toString()%></a><br>
	<a href="<%=session.getAttribute("futureMetrics").toString() %>"><%=session.getAttribute("futureMetrics").toString()%></a><br>
</body>
</html>