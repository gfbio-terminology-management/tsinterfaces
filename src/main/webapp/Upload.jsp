<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<title>Upload new Ontology</title>
<script>
function checkUpload(){
	if(!$("#myFile").val().trim()){
		alert("Please select a file to upload");
		return false;
	}
	return true;
}

</script>

</head>
<body>



<h1>Upload your Ontology</h1>
<form action="UploadData" enctype="multipart/form-data" method="POST" onsubmit="return checkUpload()">
<input type="file" name="myFile" id="myFile">
<input type="submit" value="Upload">
</form>

<% if(session.getAttribute("error")!=null){  %>
<script>alert("Unfortunately the upload was not possible due to syntax errors or inaccessibility of this or imported ontologies. Please revise the ontologies and try again. ")</script>
<%}%>

</body>
</html>