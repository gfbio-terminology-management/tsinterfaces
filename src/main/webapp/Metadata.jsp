<%@page import="org.gfbio.wrappers.OmvInfo.OmvIndividual"%>
<%@page import="java.util.HashMap"%>
<%@page import="org.gfbio.wrappers.OmvInfo"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.ArrayList"%>
<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="scripts/jquery.tagit.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="scripts/tag-it.js"></script>
<%!
//Object to extract individuals from omv graph from virtuoso
OmvInfo oi = new OmvInfo();


//Methods to extract metadata from request

String getOntologyName(HttpSession r){
	if(r.getAttribute("ontology_name") != null)
		return r.getAttribute("ontology_name").toString();
	return "";
};

String getOntologyUri(HttpSession r){
	if(r.getAttribute("ontology_uri") != null)
		return r.getAttribute("ontology_uri").toString();
	return "";
};

String getOntologyDescription(HttpSession r){
	if(r.getAttribute("ontology_description") != null)
		return r.getAttribute("ontology_description").toString();
	return "";
};

String getContributors(HttpSession r){
	if(r.getAttribute("ontology_contributors") != null)
		return r.getAttribute("ontology_contributors").toString();
	return "";
}

String getLanguage(HttpSession r){
	if(r.getAttribute("ontology_language") != null)
		return r.getAttribute("ontology_language").toString();
	return "";
}

String getOntologyFormatLangage(HttpSession r){
	if (r.getAttribute("ontology_format_language") != null)
		return r.getAttribute("ontology_format_language").toString();
	return "";
}

String getCreationDate(HttpSession r){
	if(r.getAttribute("ontology_creation_date") != null)
		return r.getAttribute("ontology_creation_date").toString();
	return "";
}

String getReleaseDate(HttpSession r){
	if(r.getAttribute("ontology_release_date") != null)
		return r.getAttribute("ontology_release_date").toString();
	return "";
}

String getCreators(HttpSession r){
	if(r.getAttribute("ontology_creators") != null)
		return r.getAttribute("ontology_creators").toString();
	return "";
}

String getLabel(HttpSession r){
	if(r.getAttribute("ontology_format_label") != null)
		return r.getAttribute("ontology_format_label").toString();
	return "";
}
String getSynonym(HttpSession r){
	if(r.getAttribute("ontology_format_synonym") != null)
		return r.getAttribute("ontology_format_synonym").toString();
	return "";
}
String getDefinition(HttpSession r){
	if(r.getAttribute("ontology_format_definition") != null)
		return r.getAttribute("ontology_format_definition").toString();
	return "";
}
String getOntologyStoreLocation(HttpSession r){
	if(r.getAttribute("ontology_store_location") != null)
		return r.getAttribute("ontology_store_location").toString();
	return "";
}
String getAcronym(HttpSession r){
	if(r.getAttribute("acronym") != null)
		return r.getAttribute("acronym").toString();
	return "";
}
String getFormalityLevel(HttpSession r){
	if(r.getAttribute("formalityLevel") != null)
		return r.getAttribute("formalityLevel").toString();
	return "";
}
String getDomains(HttpSession r){
	if(r.getAttribute("domains") != null)
		return r.getAttribute("domains").toString();
	return "";
}
String getVersion(HttpSession r){
	if(r.getAttribute("version") != null)
		return r.getAttribute("version").toString();
	return "";
}
String getKeywords(HttpSession r){
	if(r.getAttribute("keywords") != null)
		return r.getAttribute("keywords").toString();
	return "";
}
String getDocumentation(HttpSession r){
	if(r.getAttribute("documentation") != null)
		return r.getAttribute("documentation").toString();
	return "";
}
String getHomepage(HttpSession r){
	if(r.getAttribute("homepage") != null)
		return r.getAttribute("homepage").toString();
	return "";
}
String getContactFirstName(HttpSession r){
	if(r.getAttribute("contact_firstname") != null)
		return r.getAttribute("contact_firstname").toString();
	return "";
}
String getContactLastName(HttpSession r){
	if(r.getAttribute("contact_lastname") != null)
		return r.getAttribute("contact_lastname").toString();
	return "";
}
String getContactMail(HttpSession r){
	if(r.getAttribute("contact_mail") != null)
		return r.getAttribute("contact_mail").toString();
	return "";
}

String arrayToJSArray(String[] array){
	List<String> tmp = new ArrayList<String>();
	for(String s : array)
		tmp.add(s);
	return arrayToJSArray(tmp);
}


String arrayToJSArray(List<String> array){
	if(array.size() == 0)
		return "[]";
	if(array.size() == 1)
		return "[\"" + array.get(0) + "\"]";

	StringBuilder s = new StringBuilder();
	s.append("[\"");
	s.append(array.get(0));
	s.append("\"");
	for(int i = 1; i < array.size(); i++){
		s.append(',');
		s.append("\"");
		s.append(array.get(i).trim());
		s.append("\"");
	}
	s.append("]");
	return s.toString();
}

%>

<script>
	function validateEmail(email) {
	    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	    return re.test(email);
	}
	

	function validateForm(){
		var email = $("#ontology_contact_email").val();
		if(!validateEmail(email)){
			alert("please choose a valid email address");
			return false;
		}
		
		//Convert domainnames to domainuris
		//into 
		var allUris = '';
		var selectedDomains = $('#domains_tags').tagit('assignedTags');
		for(var i = 0; i < selectedDomains.length -1; i++){
			allUris +=  domains[selectedDomains[i]] + ",";
		}
		allUris += domains[selectedDomains[selectedDomains.length -1]];
		$('#ontology_domains').val(allUris);
		return true;
	}


	/*
		function to creats an autocomplete dropdown menu. The dropdown provides
		strings but is editable by user.
		@param inputID HTML ID-tag from a HTML input element
		@param data an javascript string array with which will be added to dropdown
	*/	
	function createAutocompleteDropdown(inputID, data, tooltipText){
		var jID = "#" + inputID;
		$(jID).autocomplete({source : data, minLength: 0, delay: 0,});
		var visible = false;
		var button = $( "<a>" ).attr( "tabIndex", -1 )
							   .attr( "title", tooltipText)
							   .tooltip()
							   .button({
									icons: {primary: "ui-icon-triangle-1-s"},text: false
							   })
							   .mousedown(function() {
									visible = ($(jID).autocomplete( "widget" ).is( ":visible" ));
							   })
							   .click(function(){
								   $(jID).focus();
								   if (visible)
									   return;
								   $(jID).autocomplete("search","");
							   });
		if(data.length > 0)
			$(jID).val(data[0]);
		button.insertAfter($(jID));
	};

	var domains = [];

	var keywords = [];
	<%
	for (OmvIndividual domain : oi.getOntologyDomains()){
		String domainURI = domain.getUri();
		String domainName = domain.getLabel();
		List<String> keywords = oi.getOntologyDomainKeywords(domainURI);
		out.println("keywords['" + domainName + "'] = " + arrayToJSArray(keywords) + ";");
		out.println("domains['" + domainName + "'] = '" + domainURI + "';");
	}
   %>
	
	$(document).ready(function(){
		//Create date picker for creation and release date
		$("#ontology_creation_date").datepicker();
		$("#ontology_release_date").datepicker();
		$('#upload').button();

		//Prepare widgets to select domains
		$('#domains_tags').tagit({
			singleField: true,
			readOnly : true,
		});
		var ontoDomains = <%= arrayToJSArray(getDomains(session).split(","))%>;
		for(var i = 0; i < ontoDomains.length; i++){
			$('#domains_tags').tagit('createTag',ontoDomains[i]);	
		}
		$('#ontology_domain_dropdown').change(function (){
			var selectedDomainName = this.value;
			$('#domains_tags').tagit('createTag',selectedDomainName);
		});
		for(var key in domains){
			console.log(key);
			$('#ontology_domain_dropdown').append($('<option>', { 
		        value: key,
		        text : key 
		    }));
		}
		
		
		$('#keywords_tags').tagit({
			singleField: true,
            singleFieldNode: $('#ontology_keywords'),
            availableTags : keywords[$("#ontology_domains").val()]
		});
		$('#creators_tags').tagit({
			singleField: true,
			readOnly : true,
            singleFieldNode: $('#ontology_creators')
		});
		$('#contributors_tags').tagit({
			singleField: true,
            singleFieldNode: $('#ontology_contributors')
		});

		//create autocomplete & editable dropdown menus
		var formatName = <%= arrayToJSArray(java.util.Arrays.asList(getLabel(session).split(",")))%>;
		var formatSynonym = <%= arrayToJSArray(java.util.Arrays.asList(getSynonym(session).split(",")))%>;
		var formatDefinition = <%= arrayToJSArray(java.util.Arrays.asList(getDefinition(session).split(",")))%>;

		createAutocompleteDropdown("format_label",formatName);
		createAutocompleteDropdown("format_synonym",formatSynonym);
		createAutocompleteDropdown("format_definition",formatDefinition);

		//check if all required fileds are filled before forwards user
		document.addEventListener("DOMContentLoaded", function() {
		    var elements = document.getElementsByTagName("INPUT");
		    for (var i = 0; i < elements.length; i++) {
		        elements[i].oninvalid = function(e) {
		            e.target.setCustomValidity("");
		            if (!e.target.validity.valid) {
		                e.target.setCustomValidity("This field cannot be left blank");
		            }
		        };
		        elements[i].oninput = function(e) {
		            e.target.setCustomValidity("");
		        };
		    }
		});
		
		//change provided keywords if user changed domain
		$("#ontology_domain").change(function(){
			$('#keywords_tags').tagit({
				availableTags : keywords[$("#ontology_domain").val()]
			});
		});
		
	}); //endof $(document).ready

</script>


<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Metadata</title>
</head>
<body>

<h2>Metadata</h2>
<div>
<form  action="EditTicket" method="POST" onsubmit="return validateForm()">
<fieldset style="width: 60%">
	<table border="0" cellpadding="0" cellspacing="4">
		<tr>
			<td align="left">Name*:</td>
			<td style="padding-left: 70px;"><input required title ="Please fill out this field." type="text" name="ontology_name" class="ui-widget-content ui-corner-all" size="50" value="<%=getOntologyName(session)%>"></td>
		</tr>
		<tr>
			<td align="left">URI*:</td>
			<td style="padding-left: 70px;"> <input type="text" name="ontology_uri" size="50" class="ui-widget-content ui-corner-all" value="<%=getOntologyUri(session)%>"></td>
		</tr>
		<tr>
			<td align="left">Description*:</td>
			<td style="padding-left: 70px;"><textarea required title ="Please fill out this field." class="FormElement" name="ontology_description" style="width: 498px;"  rows="4" ><%=getOntologyDescription(session)%></textarea></td>
			</tr>
		<tr>
			<td align="left">Acronym*:</td>
			<td style="padding-left: 70px;"><input required title ="Please fill out this field." type="text" class="ui-widget-content ui-corner-all" value="<%=getAcronym(session)%>" size="50" name="ontology_acronym"></td>
		</tr>
		<tr>
			<td align="left">Version:</td>
			<td style="padding-left: 70px;"><input type="text" size="50" class="ui-widget-content ui-corner-all" name="ontology_version" value="<%=getVersion(session)%>"></td>
		</tr>
		<tr>
			<td align="left">Status*:</td>
			<td style="padding-left: 70px;">
					<select required style="width:120px;" class="ui-widget-content ui-corner-all" name="ontology_status">
						<option>Alpha</option>
						<option>Beta</option>
						<option>Release</option>
						<option>Production</option>
					</select>
			</td>
		</tr>
		<tr>
			<td align="left">Domain*:</td>
                       <td style="padding-left: 70px;">
                       <select required style="width:120px;" class="ui-widget-content ui-corner-all"  id="ontology_domain_dropdown">
	                       	<option></option>
                       </select>
                   <ul id="domains_tags"></ul>
                   <input type="hidden" name="ontology_domains" id="ontology_domains">

	    </tr>
		<tr><td><br></td></tr>
	    <tr>
	    	<td align="left">Creation date:</td>
	    	<td style="padding-left: 70px;"><input name="ontology_creation_date" id="ontology_creation_date" type="text" class="ui-widget-content ui-corner-all" size="50" value="<%=getCreationDate(session)%>"></td>
	    </tr>
	    <tr>
	    	<td align="left">Release date*:</td>
	    	<td style="padding-left: 70px;"><input required title ="Please choose a release date." name="ontology_release_date" id="ontology_release_date" class="ui-widget-content ui-corner-all" type="text" size="50" value="<%=getReleaseDate(session)%>"></td>
	    </tr>
	    <tr>
			<td align="left">Keywords:</td>
			
			<td style="padding-left: 70px;">
			<ul id="keywords_tags"></ul>
			<input type="hidden" name="ontology_keywords" id="ontology_keywords" value="<%=getKeywords(session)%>">
			<br><font size="1">(separated by comma)</font></td>
		</tr>
		<tr>
			<td align="left">Language:</td>
			<td style="padding-left: 70px;"><input type="text" name="ontology_language" size="50" class="ui-widget-content ui-corner-all" value="<%=getLanguage(session)%>"></td>
		</tr>
	<tr>
		<td align="left">Creators:</td>
		<td style="padding-left: 70px;">
		<ul id="creators_tags"></ul>
		<input type="hidden" name="ontology_creators" id="ontology_creators" value="<%=getCreators(session)%>"></input><font size="1">(separated by comma)</font></td>
	</tr>
	<tr>
		<td align="left">Contributors: </td>
		<td style="padding-left: 70px;">
		<ul id="contributors_tags"></ul>
		<input type="hidden" name="ontology_contributors" id="ontology_contributors" value="<%=getContributors(session)%>"><br><font size="1">(separated by comma)</font></td>
	</tr>
	</table>
</fieldset>
<p/>
<fieldset style="width: 60%">
	<legend>Format</legend>
	<table border="0" cellpadding="0" cellspacing="4">
	    <tr>
	    	<td align="left">Label:</td> 
	    	<td style="padding-left: 61px;"><input size="50" id="format_label" class="ui-widget-content ui-corner-all" name="ontology_format_label"><br>
	    	</td>
	    </tr>
	    <tr>
	    	<td align="left">Synonym:</td>
	    	<td style="padding-left: 61px;"><input size="50" id="format_synonym" class="ui-widget-content ui-corner-all" name="ontology_format_synonym"></td>
	    </tr>
	    <tr>
	    	<td align="left">Definition:</td>
	    	<td style="padding-left: 61px;"><input size="50" id="format_definition" class="ui-widget-content ui-corner-all" name="ontology_format_definition"></td>
	    </tr>
	    <tr>
	    <td align="left">Ontology Language*:<br>
	   		<td style="padding-left: 61px;">
	   		<select style="width:120px;" class="ui-widget-content ui-corner-all" name="ontology_format_language">
				<%	
				String value = "";
				Map<String, String> ontoLangs = new HashMap<String,String>();
				ontoLangs.put("OWL", "http://omv.ontoware.org/ontology#OWL");
				ontoLangs.put("SKOS", "http://purl.org/gfbio/metadata-schema#SKOS");
				String ontologyFormatLanguage = getOntologyFormatLangage(session);
				if(ontologyFormatLanguage != null && !ontologyFormatLanguage.trim().isEmpty()){
					//Dirty as hell :(
					if(ontologyFormatLanguage.equals("http://omv.ontoware.org/ontology#OWL")){
						value = "OWL";
					}else if(ontologyFormatLanguage.equals("http://purl.org/gfbio/metadata-schema#SKOS")){
						value = "SKOS";
					}else{
						value = ontologyFormatLanguage;
					}
					out.println("<option value=\"" + ontoLangs.get(value) +  "\">" + value + "</option>");
									
				}
				for(Map.Entry<String,String> entry : ontoLangs.entrySet()){
					if(!entry.getKey().equals(value))
						out.println("<option value=\"" + entry.getValue() + "\">" + entry.getKey() + "</option>");
				}
				%>
	    </select></td>
	    </tr>
	    <tr>
	    	<td align="left">Formality level*:</td>
	    	<td style="padding-left: 61px;"><select required style="width:120px;"  class="ui-widget-content ui-corner-all" name="ontology_format_formality_level">
				<%
					String formalityLevel = getFormalityLevel(session);
						List<OmvIndividual> formalityLevels = oi.getOntologyFormalityLevels();
						if (formalityLevel.length()>0){
							for(OmvIndividual level : formalityLevels){
								if (level.getUri().equals(formalityLevel)){
									out.println("<option value=\"" + level.getUri() + "\" selected=\"selected\">" + level.getLabel() + "</option>");
								}
								else if (!level.getUri().equals(formalityLevel)){
									out.println("<option value=\"" + level.getUri() + "\">" + level.getLabel() + "</option>");
								}
							}
						}
					 else {
						out.println("<option value=\"\"></option>");
						for (OmvIndividual level : formalityLevels) {
							out.println("<option value=\"" + level.getUri() + "\">"
									+ level.getLabel() + "</option>");
						}
					}
				%>
	    	</select></td>
	    </tr>
	</table>
</fieldset>
<p/>
<fieldset style="width: 60%">
	<legend>Contact</legend>
	<table border="0" cellpadding="0" cellspacing="4">
	<tr>
		<td align="left">First Name*:</td>
		<td style="padding-left: 14px;"><input required title ="Please fill out this field." type="text" name="ontology_contact_firstname" value="<%=getContactFirstName(session)%>" class="ui-widget-content ui-corner-all" size="50"></input></td>
	</tr>
	<tr>
		<td align="left">Last Name*:</td>
		<td style="padding-left: 14px;"><input required title ="Please fill out this field." type="text" name="ontology_contact_lastname" value="<%=getContactLastName(session)%>" class="ui-widget-content ui-corner-all" size="50"></input></td>
	</tr>
	<tr>
		<td align="left">Email*:</td>
		<td style="padding-left: 14px;"><input required title ="Please fill out this field." type="text" id="ontology_contact_email" name="ontology_contact_email" value="<%=getContactMail(session)%>" class="ui-widget-content ui-corner-all" size="50"></td>
	</tr>
	<tr>
		<td align="left">Home page:</td>
		<td style="padding-left: 14px;"><input type="text" name="ontology_homepage" class="ui-widget-content ui-corner-all" size="50" value="<%=getHomepage(session)%>"></td>
	<tr>
	<tr>
		<td align="left">Documentation page:</td>
		<td style="padding-left: 14px;"><input type="text" name="ontology_documentation" class="ui-widget-content ui-corner-all" size="50" value="<%=getDocumentation(session)%>"></td>
	</tr>
	</table>
</fieldset>
<br>
<input type="submit" name="upload" value="submit" id="upload">
<br>
</form>
</div>

<% if(session.getAttribute("error")!=null){  %>
<script>alert("Unfortunately the upload was not possible due to inconsistency of the ontology.")</script>
<%}%>

</body>
</html>